import { NgIfContext } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router} from '@angular/router';
import { primeraLetraMayuscula } from 'src/app/utilidades/validadores/letraCapital';

@Component({
  selector: 'app-genero-create',
  templateUrl: './genero-create.component.html',
  styleUrls: ['./genero-create.component.css']
})
export class GeneroCreateComponent implements OnInit {

  form:FormGroup;

  constructor(private router:Router,
    private formBuilder:FormBuilder) { 

  }

  ngOnInit(): void {

    this.form=this.formBuilder.group({
      nombre:[
        //valor por defecto del campo
        '',
        //Arreglo de reglas de validación
        {validators:[
          Validators.required,
          Validators.minLength(3),
          primeraLetraMayuscula()
        ]}
    ]      
    });

  }

  nombreGetErrors(){
    let campo=this.form.get("nombre");
    if(campo.hasError("required")){
      return "El campo nombre es obligatorio";
    }
    else if(campo.hasError("minlength")){
      return "Ingresa mínimo 3 caracteres";
    }
    else if(campo.hasError("letraCapital")){
      return campo.getError("letraCapital").mensaje;
    }
  }

  onGuardar(){
    //codigo para guardar
    this.router.navigate(["/generos"]);
  }

}
