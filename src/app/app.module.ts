import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ListadoGenericoComponent } from './utilidades/listado-generico/listado-generico.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { MenuComponent } from './menu/menu.component';
import { RatingComponent } from './utilidades/rating/rating.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { ActorIndexComponent } from './actor/actor-index/actor-index.component';
import { ActorCreateComponent } from './actor/actor-create/actor-create.component';
import { CineIndexComponent } from './cine/cine-index/cine-index.component';
import { CineCreateComponent } from './cine/cine-create/cine-create.component';
import { GeneroIndexComponent } from './genero/genero-index/genero-index.component';
import { GeneroCreateComponent } from './genero/genero-create/genero-create.component';
import { PeliculaIndexComponent } from './pelicula/pelicula-index/pelicula-index.component';
import { PeliculaCreateComponent } from './pelicula/pelicula-create/pelicula-create.component';
import { ActorEditComponent } from './actor/actor-edit/actor-edit.component';
import { CineEditComponent } from './cine/cine-edit/cine-edit.component';
import { GeneroEditComponent } from './genero/genero-edit/genero-edit.component';
import { PeliculaEditComponent } from './pelicula/pelicula-edit/pelicula-edit.component';
import {ReactiveFormsModule} from "@angular/forms";
@NgModule({
  declarations: [
    AppComponent,
    ActorCreateComponent,
    ActorIndexComponent,
    CineCreateComponent,
    CineIndexComponent,
    GeneroCreateComponent,
    GeneroIndexComponent,
    ListadoGenericoComponent,
    LandingPageComponent,
    MenuComponent,
    PeliculaCreateComponent,
    PeliculaIndexComponent,
    RatingComponent,
    ActorEditComponent,
    CineEditComponent,
    GeneroEditComponent,
    PeliculaEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
