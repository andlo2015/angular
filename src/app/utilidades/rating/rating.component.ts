import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.css']
})
export class RatingComponent implements OnInit {

  @Input()
  maximumRating=5;
  @Input()
  selectedRating=0;
  currentRaiting=0;
  ratingArrange=[];
  votado=false;
  @Output()
  onRate: EventEmitter<number>=new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {
    this.ratingArrange=new Array(this.maximumRating).fill(0);
  }

  onMouseEnter(position):void{
      this.selectedRating=position+1;
  }

  onMouseLeave():void{
    this.selectedRating=this.currentRaiting;
  }

  onClick(position):void{
    this.selectedRating=position+1;
    this.currentRaiting=position+1;
    this.onRate.emit(position+1);
  }


}
