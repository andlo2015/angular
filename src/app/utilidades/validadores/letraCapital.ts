import { AbstractControl, ValidatorFn } from "@angular/forms";

export function primeraLetraMayuscula():ValidatorFn{

    return (control:AbstractControl)=>{
        const valor=<string> control.value.trim();
        
        if(!valor) return
        
        if(valor.trim().length==0) return;
        
        const primeraLetra=valor[0];
       
        if(primeraLetra!==primeraLetra.toUpperCase()){
            return {
                //letraCapital es el nombre del error
                letraCapital:{
                    mensaje:"La primera letra debe ser mayúscula"
                }
        }

    }
};
}