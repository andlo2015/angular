import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ActorCreateComponent } from './actor/actor-create/actor-create.component';
import { ActorEditComponent } from './actor/actor-edit/actor-edit.component';
import { ActorIndexComponent } from './actor/actor-index/actor-index.component';
import { CineCreateComponent } from './cine/cine-create/cine-create.component';
import { CineEditComponent } from './cine/cine-edit/cine-edit.component';
import { CineIndexComponent } from './cine/cine-index/cine-index.component';
import { GeneroCreateComponent } from './genero/genero-create/genero-create.component';
import { GeneroEditComponent } from './genero/genero-edit/genero-edit.component';
import { GeneroIndexComponent } from './genero/genero-index/genero-index.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { PeliculaCreateComponent } from './pelicula/pelicula-create/pelicula-create.component';
import { PeliculaEditComponent } from './pelicula/pelicula-edit/pelicula-edit.component';
import { PeliculaIndexComponent } from './pelicula/pelicula-index/pelicula-index.component';

const routes: Routes = [
  {path:'',component:LandingPageComponent},

  {path:'actores',component:ActorIndexComponent},
  {path:'actores/crear',component:ActorCreateComponent},
  {path:'actores/:id/editar',component:ActorEditComponent},

  {path:'cines',component:CineIndexComponent},
  {path:'cines/crear',component:CineCreateComponent},
  {path:'cines/:id/editar',component:CineEditComponent},

  {path:'generos',component:GeneroIndexComponent},
  {path:'generos/crear',component:GeneroCreateComponent},
  {path:'generos/:id/editar',component:GeneroEditComponent},

  {path:'peliculas',component:PeliculaIndexComponent},
  {path:'peliculas/crear',component:PeliculaCreateComponent},
  {path:'peliculas/:id/editar',component:PeliculaEditComponent},

  //WildCard.- Regla de ruteo que se ejecuta, cuando NO encuentra la URL.
  //Debe estar siempre al último. De lo contrario se ejecutaría en cuanto llegue a esta línes. 
  //Sin importar si hay mas rutas después.
  {path:'**',redirectTo:''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
