import { ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChange, SimpleChanges, ViewChild } from '@angular/core';
import { ListadoGenericoComponent } from 'src/app/utilidades/listado-generico/listado-generico.component';

@Component({
  selector: 'app-pelicula-index',
  templateUrl: './pelicula-index.component.html',
  styleUrls: ['./pelicula-index.component.css']
})
export class PeliculaIndexComponent implements OnInit {

  @Input()
  peliculas;

  constructor() { }

  ngOnInit(): void {
    
  }


  onEliminar(position:number):void{
    this.peliculas.splice(position,1);
  }

}
